package com.example.mc.niezbednikglobtrotera;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;


public class HelpActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.showOverflowMenu();


    }

    public void MainActivity(View v) {

        if (v.getId() == R.id.button7) {
            Intent i = new Intent(HelpActivity.this, MainActivity.class);
            startActivity(i);
        }
    }
}