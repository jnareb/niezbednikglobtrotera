package com.example.mc.niezbednikglobtrotera;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class WeatherActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, LocationListener {

    List<String> cities;
    List<String> latitudes;
    List<String> longitudes;
    public String URL_TO_HIT;
    private ProgressDialog dialog;
    LocationManager locationManager;
    String provider;
    double lat, lon;

    ArrayList<String> day = new ArrayList<>();
    ArrayList<String> month = new ArrayList<>();
    ArrayList<String> conditions = new ArrayList<>();
    ArrayList<String> max = new ArrayList<>();
    ArrayList<String> min = new ArrayList<>();
    ArrayList<String> rain = new ArrayList<>();
    ArrayList<String> wind = new ArrayList<>();
    ArrayList<String> year = new ArrayList<>();
    ArrayList<String> humidity = new ArrayList<>();
    Location location;
    ImageView ivIcon, ivIcon2, ivIcon3;
    TextView tvDate, tvMax, tvMin, tvRain, tvWind, tvHum;
    TextView tvDate2, tvMax2, tvMin2, tvRain2, tvWind2, tvHum2;
    TextView tvDate3, tvMax3, tvMin3, tvRain3,tvWind3, tvHum3;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

           setContentView(R.layout.activity_weather);


        AssetManager assetManager = getAssets();
        String first = "Moja lokalizacja";


        dialog = new ProgressDialog(this);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setMessage("Ładowanie danych");

        ivIcon = (ImageView) findViewById(R.id.IvIcon);
        tvDate = (TextView) findViewById(R.id.TvDate);
        tvMax = (TextView) findViewById(R.id.TvMax);
        tvMin = (TextView) findViewById(R.id.TvMin);
        tvRain = (TextView) findViewById(R.id.TvRain);
        tvWind = (TextView) findViewById(R.id.TvWind);
        tvHum = (TextView) findViewById(R.id.TvHum);

        ivIcon2 = (ImageView) findViewById(R.id.IvIcon2);
        tvDate2 = (TextView) findViewById(R.id.TvDate2);
        tvMax2 = (TextView) findViewById(R.id.TvMax2);
        tvMin2 = (TextView) findViewById(R.id.TvMin2);
        tvRain2 = (TextView) findViewById(R.id.TvRain2);
        tvWind2 = (TextView) findViewById(R.id.TvWind2);
        tvHum2 = (TextView) findViewById(R.id.TvHum2);

        ivIcon3 = (ImageView) findViewById(R.id.IvIcon3);
        tvDate3 = (TextView) findViewById(R.id.TvDate3);
        tvMax3 = (TextView) findViewById(R.id.TvMax3);
        tvMin3 = (TextView) findViewById(R.id.TvMin3);
        tvRain3 = (TextView) findViewById(R.id.TvRain3);
        tvWind3 = (TextView) findViewById(R.id.TvWind3);
        tvHum3 = (TextView) findViewById(R.id.TvHum3);


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        }

        Criteria criteria = new Criteria();


        provider = locationManager.getBestProvider(criteria, false);

        if (provider != null && !provider.equals("")) {


            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider, 300000, 1, this);

            if (location != null) {
                onLocationChanged(location);
            }

        }



        InputStream input;
        try {
            input = assetManager.open("cities.txt");

            int size = input.available();
            byte[] buffer = new byte[size];
            input.read(buffer);
            input.close();


            String text = new String(buffer);


            cities = new ArrayList<>(Arrays.asList(text.split("\n")));
            cities.add(0, first);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        InputStream input2;
        try {
            input2 = assetManager.open("latitudes.txt");

            int size2 = input2.available();
            byte[] buffer2 = new byte[size2];
            input2.read(buffer2);
            input2.close();


            String text2 = new String(buffer2);


            latitudes = new ArrayList<>(Arrays.asList(text2.split("\n")));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        InputStream input3;
        try {
            input3 = assetManager.open("longitudes.txt");

            int size3 = input3.available();
            byte[] buffer3 = new byte[size3];
            input3.read(buffer3);
            input3.close();

            String text3 = new String(buffer3);


            longitudes = new ArrayList<>(Arrays.asList(text3.split("\n")));

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Toolbar toolbar;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.showOverflowMenu();


        Spinner spinner = (Spinner) findViewById(R.id.spinner3);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter(this, R.layout.spinner_item, cities);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (location != null) {
            if (position == 0) {
                lat = location.getLatitude();
                lon = location.getLongitude();
            } else {
                lat = Double.parseDouble(latitudes.get(position));
                lon = Double.parseDouble(longitudes.get(position));
            }

            URL_TO_HIT = "http://api.wunderground.com/api/8106b7128208658f/forecast/q/" + lat + "," + lon + ".json";
            new JSONTask().execute(URL_TO_HIT);


        }
    }

    public class JSONTask extends AsyncTask<String, Void, ArrayList<String>> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();

            day.clear();
            month.clear();
            conditions.clear();
            max.clear();
            min.clear();
            wind.clear();
            rain.clear();
            humidity.clear();
            year.clear();

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder buffer = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }

                JSONObject finalJson = new JSONObject(buffer.toString());
                JSONObject forecast = finalJson.getJSONObject("forecast");
                JSONObject simple_forecast = forecast.getJSONObject("simpleforecast");
                JSONArray forecastday = simple_forecast.getJSONArray("forecastday");

                for (int i = 1; i < forecastday.length(); i++) {

                    JSONObject child_object = forecastday.getJSONObject(i);
                    JSONObject date = child_object.getJSONObject("date");
                    day.add(String.valueOf(date.getInt("day")));
                    month.add(String.valueOf(date.getInt("month")));
                    year.add(String.valueOf(date.getInt("year")));

                    JSONObject high = child_object.getJSONObject("high");
                    max.add(String.valueOf(high.getInt("celsius")));

                    conditions.add(child_object.getString("conditions"));

                    JSONObject low = child_object.getJSONObject("low");
                    min.add(String.valueOf(low.getInt("celsius")));

                    JSONObject qpf_allday = child_object.getJSONObject("qpf_allday");
                    rain.add(String.valueOf(qpf_allday.getDouble("mm")));

                    JSONObject avewind = child_object.getJSONObject("avewind");
                    wind.add(String.valueOf(avewind.getInt("kph")));

                    humidity.add(String.valueOf(child_object.getInt("avehumidity")));


                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(final ArrayList<String> result) {
            super.onPostExecute(result);

            if (!day.get(0).equals("") && !month.get(0).equals("")) {


                if (Double.valueOf(month.get(0)) < 10) {
                    tvDate.setText(day.get(0) + "." + "0" + month.get(0) + "." + year.get(0));
                } else {
                    tvDate.setText(day.get(0) + "." + month.get(0));
                }
            } else {
                tvDate.setText(R.string.no_data);
            }

            if (!max.get(0).equals("")) {

                tvMax.setText("max: " + max.get(0) + "\u00B0" + "C");
            } else {
                tvMax.setText(R.string.no_data);
            }

            if (!min.get(0).equals("")) {

                tvMin.setText("min: " + min.get(0) + "\u00B0" + "C");
            } else {
                tvMin.setText(R.string.no_data);
            }

            if (!rain.get(0).equals("")) {

                tvRain.setText("opad: " + rain.get(0) + " mm");
            } else {
                tvRain.setText(R.string.no_data);
            }

            if (!wind.get(0).equals("")) {

                tvWind.setText("wiatr: " + wind.get(0) + " km/h");
            } else {
                tvWind.setText(R.string.no_data);
            }

            if (!humidity.get(0).equals("")) {

                tvHum.setText("wilgotność: " + humidity.get(0) + " %");
            } else {
                tvHum.setText(R.string.no_data);
            }

            // obrazy pogodowe

            if (conditions.get(0).equals("Clear")) {
                ivIcon.setImageResource(R.drawable.clear);
            }

            if (conditions.get(0).equals("Partly Cloudy")) {
                ivIcon.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(0).equals("Scattered Clouds")) {
                ivIcon.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(0).equals("Overcast")) {
                ivIcon.setImageResource(R.drawable.overcast);
            }
            if (conditions.get(0).equals("Mostly Cloudy")) {
                ivIcon.setImageResource(R.drawable.cloudy);
            }
            if (conditions.get(0).equals("Light Rain Showers")) {
                ivIcon.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(0).equals("Light Rain")) {
                ivIcon.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(0).equals("Thunderstorm")) {
                ivIcon.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(0).equals("Chance of a Thunderstorm")) {
                ivIcon.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(0).equals("Chance of Rain")) {
                ivIcon.setImageResource(R.drawable.chance_of_rain);
            }
            if (conditions.get(0).equals("Rain")) {
                ivIcon.setImageResource(R.drawable.rain);
            }

            // 2 DZIEN

            if (!day.get(1).equals("") && !month.get(1).equals("")) {


                if (Double.valueOf(month.get(1)) < 10) {
                    tvDate2.setText(day.get(1) + "." + "0" + month.get(1) + "." + year.get(1));
                } else {
                    tvDate2.setText(day.get(1) + "." + month.get(1));
                }
            } else {
                tvDate2.setText(R.string.no_data);
            }

            if (!max.get(1).equals("")) {

                tvMax2.setText("max: " + max.get(1) + "\u00B0" + "C");
            } else {
                tvMax2.setText(R.string.no_data);
            }

            if (!min.get(1).equals("")) {

                tvMin2.setText("min: " + min.get(1) + "\u00B0" + "C");
            } else {
                tvMin2.setText(R.string.no_data);
            }

            if (!rain.get(1).equals("")) {

                tvRain2.setText("opad: " + rain.get(1) + " mm");
            } else {
                tvRain2.setText(R.string.no_data);
            }

            if (!wind.get(1).equals("")) {

                tvWind2.setText("wiatr: " + wind.get(1) + " km/h");
            } else {
                tvWind2.setText(R.string.no_data);
            }

            if (!humidity.get(1).equals("")) {

                tvHum2.setText("wilgotność: " + humidity.get(1) + " %");
            } else {
                tvHum2.setText(R.string.no_data);
            }

            // obrazy pogodowe

            if (conditions.get(1).equals("Clear")) {
                ivIcon2.setImageResource(R.drawable.clear);
            }

            if (conditions.get(1).equals("Partly Cloudy")) {
                ivIcon2.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(1).equals("Scattered Clouds")) {
                ivIcon2.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(1).equals("Overcast")) {
                ivIcon2.setImageResource(R.drawable.overcast);
            }
            if (conditions.get(1).equals("Mostly Cloudy")) {
                ivIcon2.setImageResource(R.drawable.cloudy);
            }
            if (conditions.get(1).equals("Light Rain Showers")) {
                ivIcon2.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(1).equals("Light Rain")) {
                ivIcon2.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(1).equals("Thunderstorm")) {
                ivIcon2.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(1).equals("Chance of a Thunderstorm")) {
                ivIcon2.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(1).equals("Chance of Rain")) {
                ivIcon2.setImageResource(R.drawable.chance_of_rain);
            }
            if (conditions.get(1).equals("Rain")) {
                ivIcon2.setImageResource(R.drawable.rain);
            }

            // 3 DZIEN

            if (!day.get(2).equals("") && !month.get(2).equals("")) {


                if (Double.valueOf(month.get(2)) < 10) {
                    tvDate3.setText(day.get(2) + "." + "0" + month.get(2) + "." + year.get(2));
                } else {
                    tvDate3.setText(day.get(2) + "." + month.get(2)+"."+year.get(2));
                }
            } else {
                tvDate3.setText(R.string.no_data);
            }

            if (!max.get(2).equals("")) {

                tvMax3.setText("max: " + max.get(2) + "\u00B0" + "C");
            } else {
                tvMax3.setText(R.string.no_data);
            }

            if (!min.get(2).equals("")) {

                tvMin3.setText("min: " + min.get(2) + "\u00B0" + "C");
            } else {
                tvMin3.setText(R.string.no_data);
            }

            if (!rain.get(2).equals("")) {

                tvRain3.setText("opad: " + rain.get(2) + " mm");
            } else {
                tvRain3.setText(R.string.no_data);
            }

            if (!wind.get(2).equals("")) {

                tvWind3.setText("wiatr: " + wind.get(2) + " km/h");
            } else {
                tvWind3.setText(R.string.no_data);
            }

            if (!humidity.get(2).equals("")) {

                tvHum3.setText("wilgotność: " + humidity.get(2) + " %");
            } else {
                tvHum3.setText(R.string.no_data);
            }

            // obrazy pogodowe

            if (conditions.get(2).equals("Clear")) {
                ivIcon3.setImageResource(R.drawable.clear);
            }
            if (conditions.get(2).equals("Partly Cloudy")) {
                ivIcon3.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(2).equals("Scattered Clouds")) {
                ivIcon3.setImageResource(R.drawable.partly_cloudy);
            }
            if (conditions.get(2).equals("Overcast")) {
                ivIcon3.setImageResource(R.drawable.overcast);
            }
            if (conditions.get(2).equals("Mostly Cloudy")) {
                ivIcon3.setImageResource(R.drawable.cloudy);
            }
            if (conditions.get(2).equals("Light Rain Showers")) {
                ivIcon3.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(2).equals("Light Rain")) {
                ivIcon3.setImageResource(R.drawable.rain_showers);
            }
            if (conditions.get(2).equals("Thunderstorm")) {
                ivIcon3.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(2).equals("Chance of a Thunderstorm")) {
                ivIcon3.setImageResource(R.drawable.thunderstorm);
            }
            if (conditions.get(2).equals("Chance of Rain")) {
                ivIcon3.setImageResource(R.drawable.chance_of_rain);
            }
            if (conditions.get(2).equals("Rain")) {
                ivIcon3.setImageResource(R.drawable.rain);
            }

        dialog.dismiss();

        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}