package com.example.mc.niezbednikglobtrotera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;


public class PlacesFragment extends Fragment implements AdapterView.OnItemSelectedListener, LocationListener {

    public PlacesFragment() {
        // Required empty public constructor
    }

    List<String> categories = new ArrayList<>();
    Context context;
    View rootView;
    Button add_button;
    LocationManager locationManager;
    String provider, text;
    Location location;
    Double lat, lon;
    ImageView icon;
    int index;
    EditText edittext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getContext();

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        }
        // Creating an empty criteria object
        Criteria criteria = new Criteria();

        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);

        if(provider!=null && !provider.equals("")){

            // Get the location from the given provider
            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider,  10000, 1, this);

            if(location!=null) {


                onLocationChanged(location);
            }
            else
                Toast.makeText(getContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(getContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        categories.add("Jedzenie");
        categories.add("Natura");
        categories. add("Transport");
        categories.add("Warto zobaczyć");
        categories.add("Wypoczynek");
        categories.add("Nocleg");

        rootView = inflater.inflate(R.layout.fragment_places, container, false);
        Spinner spinner = (Spinner) rootView.findViewById(R.id.spinner4);
        ArrayAdapter<String> dataAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_item2, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(this);
        final String file_name = "places";

        edittext = (EditText) rootView.findViewById(R.id.editText);
        icon = (ImageView) rootView.findViewById(R.id.imageView);
        add_button = (Button) rootView.findViewById(R.id.button3);
        add_button.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                lat = location.getLatitude();
                lon = location.getLongitude();
                text = edittext.getText().toString();;

                if(!fileExistance(file_name))
                {

                    File file2 = new File("data/data/com.example.mc.niezbednikglobtrotera/" + file_name);
                    if (!file2.exists()) {
                        try {
                            file2.createNewFile();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }



                    if (text.equals(""))
                    {
                        new AlertDialog.Builder(context)
                                .setTitle("Ostrzeżenie")
                                .setMessage("Dodaj opis miejsca!")
                                .setPositiveButton("Anuluj", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        dialog.cancel();
                                    }
                                })

                                .show();

                    }
                    if (!text.equals("")) {

                        new AlertDialog.Builder(context)
                                .setTitle("Ostrzeżenie")
                                .setMessage("Czy na pewno chcesz dodać miejsce?")
                                .setPositiveButton("Tak", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        try {
                                            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput(file_name, Context.MODE_APPEND));
                                            JSONObject object = new JSONObject();
                                            object.put("lat", lat).toString();
                                            object.put("lon", lon);
                                            object.put("opis", text);
                                            object.put("kategoria", index);

                                            outputStreamWriter.write(object.toString() + "," + "\r\n");
                                            outputStreamWriter.close();
                                            edittext.setText("");
                                            Toast.makeText(getActivity(), "Pomyślnie dodano miejsce",Toast.LENGTH_LONG).show();
                                            Log.e("SPRAWDZENIE ZAPISU", object.toString());
                                        }

                                        catch (IOException e) {

                                            Log.e("Exception", "File write failed: " + e.toString());

                                        } catch (JSONException e) {

                                            e.printStackTrace();
                                        }

                                    }
                                })



                        .setNegativeButton("Nie", null)
                                .show();


                    }



            }});



        return rootView;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


        if (position == 0) {

            icon.setImageResource(R.drawable.marker);
            index = 0;
        }

        else if (position == 1) {

            icon.setImageResource(R.drawable.marker2);
            index = 1;
        }

        else if (position == 2) {

            icon.setImageResource(R.drawable.marker3);
            index = 2;
        }

        else if (position == 3) {

            icon.setImageResource(R.drawable.marker4);
            index = 3;
        }
        else if (position == 4) {

            icon.setImageResource(R.drawable.marker9);
            index = 4;
        }

        else if (position == 5) {

            icon.setImageResource(R.drawable.marker10);
            index = 5;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

        index = 0;
        icon.setImageResource(R.drawable.marker);
    }


    public boolean fileExistance(String fname){
        File file = getContext().getFileStreamPath(fname);
        return file.exists();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}