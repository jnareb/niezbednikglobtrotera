package com.example.mc.niezbednikglobtrotera;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class StatisticsFragment extends Fragment
        implements
        LocationListener,
        ActivityCompat.OnRequestPermissionsResultCallback {


    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    public TextView tvSunrise, tvSunset, tvDistance, tvMoonrise, tvMoonset, tvIlluminate, tvElevation;
    public ImageView ivMoon;
    public TextView tvLongitude, tvLatitude, tvTemp, tvFeel, tvHumidity, tvPressure, tvRain, tvWind;
    public ImageView ivWeather;
    public View rootView;
    double lon, lat;
    private String URL_TO_HIT;
    ArrayList<String> weather = new ArrayList<>();
    LocationManager locationManager;
    Location location;
    String provider;
    Chronometer mChronometer;
    private int mInterval = 30000;
    private Handler mHandler;
    public Intent intent;
    int k = 0;
    public boolean networkAvailable = false;
    double distance = 0;
    List<Double> lats = new ArrayList<>();
    List<Double> lons = new ArrayList<>();


    public StatisticsFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        networkAvailable = isNetworkConnected();

        // Getting and configuring LocationManager object
        // see also:
        //   https://developer.android.com/training/location/receive-location-updates.html
        //   https://developers.google.com/maps/documentation/android-api/location
        //   https://github.com/googlemaps/android-samples/blob/master/ApiDemos/app/src/main/java/com/example/mapdemo/MyLocationDemoActivity.java
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M) {
            // Permissions from AndroidManifest are granted on install
            configureLocationProvider();
        } else {
            // Check if user granted permissions
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    ||
                ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                // We have correct user-granted runtime permissions
                configureLocationProvider();
            } else {
                // Permission to access the location is missing.
                // Request permission from user (Android 6.0)
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }

        // Does not use and does not need location
        startRepeatingTask();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationManager.removeUpdates(this);
        mHandler.removeCallbacks(mStatusChecker);
    }

    void configureLocationProvider() {
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);

        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        provider = locationManager.getBestProvider(criteria, false);

        if (provider != null && !provider.equals("")) {

            try {
                location = locationManager.getLastKnownLocation(provider);
                locationManager.requestLocationUpdates(provider, 10000, 0, this);
            } catch (Exception e) {
                location = null;
            }

            if (location!=null) {
                lat = location.getLatitude();
                lon = location.getLongitude();
                onLocationChanged(location);
                mHandler = new Handler();
            } else {
                ShowLocationDialog();
            }

        } else {
            Toast.makeText(getContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != MY_PERMISSIONS_REQUEST_LOCATION) {
            return;
        }

        if (isPermissionGranted(permissions, grantResults,
                android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            configureLocationProvider();
        } else {
            ShowLocationDialog();
        }
    }

    /**
     * Checks if the result contains a {@link PackageManager#PERMISSION_GRANTED} result for a
     * permission from a runtime permissions request.
     *
     * @see android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback
     *
     * from https://github.com/googlemaps/android-samples/blob/master/ApiDemos/app/src/main/java/com/example/mapdemo/MyLocationDemoActivity.java
     */
    private static boolean isPermissionGranted(String[] grantPermissions,
                                               int[] grantResults,
                                               String permission) {
        for (int i = 0; i < grantPermissions.length; i++) {
            if (permission.equals(grantPermissions[i])) {
                return grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
        }
        return false;
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {



        if (!networkAvailable) {
            ShowInternetDialog();
        } else {
            URL_TO_HIT = "http://api.wunderground.com/api/8106b7128208658f/conditions/astronomy/q/" + location.getLatitude() + "," + location.getLongitude() + ".json";
            new JSONTask().execute(URL_TO_HIT);
        }
        mHandler.postDelayed(mStatusChecker, mInterval);

        }
    };

    void startRepeatingTask() {
        mStatusChecker.run();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


            rootView = inflater.inflate(R.layout.fragment_statistics, container, false);



        tvSunrise = (TextView) rootView.findViewById(R.id.TvSunrise);
        tvSunset = (TextView) rootView.findViewById(R.id.TvSunset);
        tvMoonrise = (TextView) rootView.findViewById(R.id.TvMoonrise);
        tvMoonset = (TextView) rootView.findViewById(R.id.TvMoonset);
        tvIlluminate = (TextView) rootView.findViewById(R.id.TvIlluminate);
        tvElevation = (TextView) rootView.findViewById(R.id.TvElevation);
        ivMoon = (ImageView) rootView.findViewById(R.id.IvMoon);
        tvLatitude = (TextView) rootView.findViewById(R.id.TvLatitude);
        tvLongitude = (TextView) rootView.findViewById(R.id.TvLongitude);
        tvTemp = (TextView) rootView.findViewById(R.id.TvTemp);
        tvFeel = (TextView) rootView.findViewById(R.id.TvFeel);
        tvPressure = (TextView) rootView.findViewById(R.id.TvPressure);
        tvHumidity = (TextView) rootView.findViewById(R.id.TvHumidity);
        tvRain = (TextView) rootView.findViewById(R.id.TvRain);
        ivWeather = (ImageView) rootView.findViewById(R.id.IvWeather);
        tvWind = (TextView) rootView.findViewById(R.id.TvWind);
        tvDistance = (TextView) rootView.findViewById(R.id.TvDistance);

        tvDistance.setText(R.string.start_distance);

        intent = new Intent(getActivity(), WeatherActivity.class);
        final LinearLayout weather_layout = (LinearLayout) rootView.findViewById(R.id.weather_layout);

        weather_layout.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                startActivity(intent);
            }
        });



        URL_TO_HIT = "http://api.wunderground.com/api/8106b7128208658f/conditions/astronomy/q/" + 53.637848 + "," + 18.534783 + ".json";
       tvLatitude.setText("53.637848 N");
        tvLongitude.setText("18.534783 E");

        if (!networkAvailable) {
            ShowInternetDialog();
        } else {
            new JSONTask().execute(URL_TO_HIT);
        }

        mChronometer = (Chronometer) rootView.findViewById(R.id.chronometer);
        mChronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h   = (int)(time /3600000);
                int m = (int)(time - h*3600000)/60000;
                int s= (int)(time - h*3600000- m*60000)/1000 ;
                String hh = h < 10 ? "0"+h: h+"";
                String mm = m < 10 ? "0"+m: m+"";
                String ss = s < 10 ? "0"+s: s+"";
                cArg.setText(hh+":"+mm+":"+ss);
            }
        });
        mChronometer.setBase(SystemClock.elapsedRealtime());
        mChronometer.start();

        return rootView;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }



    public void ShowInternetDialog()  {
        if (!isNetworkConnected()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Brak połaczenia z internetem!")
                    .setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        }
                    })
                    .setNegativeButton("ANULUJ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                        }
                    });

            AlertDialog internet_dialog = builder.create();
            internet_dialog.show();

        }
    }

    public void ShowLocationDialog()  {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Brak sygnalu GPS. Dane mogą być nieaktualne. ")
                .setPositiveButton(R.string.settings, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("ANULUJ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });

        AlertDialog location_dialog = builder.create();
        location_dialog.show();
    }




    public class JSONTask extends AsyncTask<String,Void, ArrayList<String> > {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weather.clear();

        }

        @Override
        protected ArrayList<String> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder buffer = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }
                JSONObject finalJson = new JSONObject(buffer.toString());
                JSONObject moon_phase  = finalJson.getJSONObject("moon_phase");
                weather.add(moon_phase.getString("percentIlluminated"));
                weather.add(moon_phase.getString("phaseofMoon"));


                JSONObject sun_phase  = finalJson.getJSONObject("sun_phase");
                JSONObject sunrise  = sun_phase.getJSONObject("sunrise");
                weather.add(sunrise.getString("hour"));
                weather.add(sunrise.getString("minute"));

                JSONObject sunset  = sun_phase.getJSONObject("sunset");
                weather.add(sunset.getString("hour"));
                weather.add(sunset.getString("minute"));


                JSONObject moonrise  = moon_phase.getJSONObject("moonrise");
                weather.add(moonrise.getString("hour"));
                weather.add(moonrise.getString("minute"));

                JSONObject moonset  = moon_phase.getJSONObject("moonset");
                weather.add(moonset.getString("hour"));
                weather.add(moonset.getString("minute"));

                JSONObject current_observation  = finalJson.getJSONObject("current_observation");
                JSONObject display_location = current_observation.getJSONObject("display_location");
                weather.add(String.valueOf(display_location.getInt("elevation")));


                weather.add(String.valueOf(current_observation.getDouble("temp_c")));
                weather.add(String.valueOf(current_observation.getDouble("feelslike_c")));
                weather.add(String.valueOf(current_observation.getInt("pressure_mb")));
                weather.add(current_observation.getString("relative_humidity"));
                weather.add(String.valueOf(current_observation.getInt("precip_today_metric")));
                weather.add(current_observation.getString("weather"));
                weather.add(current_observation.getString("wind_kph"));

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                if(connection != null) {
                    connection.disconnect();
                }
                try {
                    if(reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return  null;
        }

        @Override
        protected void onPostExecute(final ArrayList<String> result) {
            super.onPostExecute(result);


            setTextToTime(tvSunrise,  weather.get(2), weather.get(3));
            setTextToTime(tvSunset,   weather.get(4), weather.get(5));

            setTextToTime(tvMoonrise, weather.get(6), weather.get(7));
            setTextToTime(tvMoonset,  weather.get(8), weather.get(9));

            setTextToData(tvIlluminate, R.string.illumination_percent_format, weather.get(0));
            setTextToData(tvElevation, R.string.elevation_value_format, weather.get(10));
            setTextToData(tvTemp, R.string.temperature_celsius_format, weather.get(11));
            setTextToData(tvFeel, R.string.feel_temperature_format, weather.get(12));
            setTextToData(tvPressure, R.string.pressure_value_format, weather.get(13));
            setTextToData(tvHumidity, R.string.humidity_value_format, weather.get(14));
            setTextToData(tvRain, R.string.rain_value_format, weather.get(15));


            // obrazy fazy ksiezyca
            switch (weather.get(1)) {
                case "First Quarter":
                    ivMoon.setImageResource(R.drawable.first_quarter);
                    break;
				case "Full":
	                ivMoon.setImageResource(R.drawable.full_moon);
                    break;
				case "Last Quarter":
	                ivMoon.setImageResource(R.drawable.last_quarter);
                    break;
				case "New":
	                ivMoon.setImageResource(R.drawable.new_moon);
                    break;
				case "Waning Gibbous":
	                ivMoon.setImageResource(R.drawable.waning_gibbous);
                    break;
				case "Waning Crescent":
	                ivMoon.setImageResource(R.drawable.waning_crescent);
                    break;
				case "Waxing Crescent":
	                ivMoon.setImageResource(R.drawable.waxing_crescent);
                    break;
				case "Waxing Gibbous":
	                ivMoon.setImageResource(R.drawable.waxing_gibbous);
                    break;
            }

            // obrazy pogodowe
            switch (weather.get(16)) {
                case "Clear":
                    ivWeather.setImageResource(R.drawable.clear);
                    break;
				case "Partly Cloudy":
	                ivWeather.setImageResource(R.drawable.partly_cloudy);
                    break;
				case "Scattered Clouds":
	                ivWeather.setImageResource(R.drawable.partly_cloudy);
                    break;
				case "Overcast":
	                ivWeather.setImageResource(R.drawable.overcast);
                    break;
				case "Mostly Cloudy":
	                ivWeather.setImageResource(R.drawable.cloudy);
                    break;
				case "Light Rain Showers":
	                ivWeather.setImageResource(R.drawable.rain_showers);
                    break;
				case "Light Rain":
	                ivWeather.setImageResource(R.drawable.rain_showers);
                    break;
				case "Thunderstorm":
	                ivWeather.setImageResource(R.drawable.thunderstorm);
                    break;
				case "Chance of a Thunderstorm":
	                ivWeather.setImageResource(R.drawable.thunderstorm);
                    break;
				case "Chance of Rain":
	                ivWeather.setImageResource(R.drawable.chance_of_rain);
                    break;
				case "Rain":
	                ivWeather.setImageResource(R.drawable.rain);
                    break;
            }

            setTextToData(tvWind, R.string.wind_value_format, weather.get(17));

            setLatLng(lat, lon);
        }
    }

    private void setTextToData(TextView textView, int formatResid, String value) {
        if (!value.equals("")) {
            textView.setText(String.format(getString(formatResid), value));
        } else {
            textView.setText(R.string.no_data);
        }
    }

    private void setTextToTime(TextView textView, String hoursString, String minutesString) {
        if (!hoursString.equals("") && !minutesString.equals("")) {
            int hours   = Integer.valueOf(hoursString,   10);
            int minutes = Integer.valueOf(minutesString, 10);
            // default format is "%02d:%02d", that is HH:MM
            textView.setText(String.format(getString(R.string.hours_minutes_format), hours, minutes));
        } else {
            textView.setText(R.string.no_data);
        }
    }

    @SuppressLint("DefaultLocale")
    private void setLatLng(double lat, double lon) {
        if (lat > 0) {
            tvLatitude.setText(String.format("%.5f E", lat));
        } else if (lat < 0){
            tvLatitude.setText(String.format("%.5f W", lat));
        } else {
            tvLatitude.setText(String.format("%.5f", lat));
        }

        if (lon > 0) {
            tvLongitude.setText(String.format("%.5f N", lon));
        } else if (lon < 0) {
            tvLongitude.setText(String.format("%.5f S", lon));
        } else {
            tvLongitude.setText(String.format("%.5f", lon));
        }
    }

    @Override
    public void onLocationChanged(Location location) {



        lat = location.getLatitude();
        lon = location.getLongitude();

        lats.add(lat);
        lons.add(lon);


        if (lats.size() >= 2 && lons.size() >= 2)

        {
                Location locationA = new Location(location);
                locationA.setLatitude(lats.get(k));
                locationA.setLongitude(lons.get(k));
                Location locationB = new Location(location);
                locationB.setLatitude(lats.get(k+1));
                locationB.setLongitude(lons.get(k+1));
                double dist = locationA.distanceTo(locationB);
                distance = distance + dist;
                k++;


            tvDistance.setText(String.format(getString(R.string.distance_km_value_format), distance/1000));
        }


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onPause() {
        super.onPause();

    }

}