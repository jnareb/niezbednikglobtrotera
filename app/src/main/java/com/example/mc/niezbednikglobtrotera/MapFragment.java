package com.example.mc.niezbednikglobtrotera;


import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


public class MapFragment extends Fragment implements LocationListener, GoogleMap.OnMarkerClickListener {



    double lon;
    double lat;
    LocationManager locationManager;
    String provider;
    Location location;
    public Handler mHandler;
    List<LatLng> routePoints = new ArrayList<>();
    List<Double> lats = new ArrayList<>();
    List<String> routes = new ArrayList<>();
    List<String> values = new ArrayList<>();
    List<Double> lons = new ArrayList<>();
    List<String> texts = new ArrayList<>();
    List<Double> categories = new ArrayList<>();
    GoogleMap map;
    int check = 1;
    public Runnable mTicker;
    ArrayList<Marker> mMarkerArray = new ArrayList<>();


    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        MapsInitializer.initialize(getContext());

        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        }
        // Creating an empty criteria object
        Criteria criteria = new Criteria();

        // Getting the name of the provider that meets the criteria
        provider = locationManager.getBestProvider(criteria, false);

        if(provider!=null && !provider.equals("")){

            // Get the location from the given provider
            location = locationManager.getLastKnownLocation(provider);
            locationManager.requestLocationUpdates(provider,  10000, 1, this);

            if(location!=null) {

                lat = location.getLatitude();
                lon = location.getLongitude();


            }
            else
                Toast.makeText(getContext(), "Location can't be retrieved", Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(getContext(), "No Provider Found", Toast.LENGTH_SHORT).show();
        }



    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_map, container, false);

        SupportMapFragment mSupportMapFragment;
        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mSupportMapFragment == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            mSupportMapFragment = SupportMapFragment.newInstance();
            fragmentTransaction.replace(R.id.map, mSupportMapFragment).commit();
        }

        if (mSupportMapFragment != null)
        {
            mSupportMapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {

                    map = googleMap;
                    if (googleMap != null) {

                        googleMap.getUiSettings().setAllGesturesEnabled(true);

                        if (location!=null)
                        {
                            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15.0f).build();
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.moveCamera(cameraUpdate);
                        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                || ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                            googleMap.setMyLocationEnabled(true);
                            googleMap.getUiSettings().setZoomControlsEnabled(true);
                            onLocationChanged(location);
                        }
                        }

                    }

                }
            });


            mHandler = new Handler();
           mTicker = new Runnable() {

                @Override
                public void run() {

                    readFile(map);
                    mHandler.postDelayed(mTicker, 10000);

                }
            };

            mHandler.postDelayed(mTicker, 10000);


           final Button tracing = (Button) v.findViewById(R.id.button3);
            tracing.setOnClickListener( new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if(check==1)
                    {
                        tracing.setBackground(getActivity().getResources().getDrawable(R.drawable.pause));
                        mHandler = new Handler();
                        mTicker = new Runnable() {

                            @Override
                            public void run() {

                                drawRoute(map);
                                mHandler.postDelayed(mTicker, 10000);

                            }
                        };

                        check = 2;
                    }

                   else if (check == 2)
                    {
                        tracing.setBackground(getActivity().getResources().getDrawable(R.drawable.play));
                        check = 1;
                    }



                }
            });

            final Button save_button = (Button) v.findViewById(R.id.button);

            save_button.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {


                    String toJSON = "";
                    String sdate, smonth, syear, shour, sminute, ssecond;
                    Calendar calendar = Calendar.getInstance();

                    int date = calendar.get(Calendar.DATE);
                    int month = (calendar.get(Calendar.MONTH)+1);
                    int year = calendar.get(Calendar.YEAR);
                    int hour = (calendar.get(Calendar.HOUR_OF_DAY) +1);
                    int minute = calendar.get(Calendar.MINUTE);
                    int second = calendar.get(Calendar.SECOND);

                    sdate = date < 10 ? "0" + date: date + "";
                    smonth = month < 10 ? "0"+ month: month + "";
                    syear = year < 10 ? "0" + year: year + "";
                    shour = hour < 10 ? "0" + hour: hour + "";
                    sminute = minute < 10 ? "0" + minute: minute + "";
                    ssecond = second < 10 ? "0" + second: second + "";

                    String current_date = sdate + "-" + smonth + "-" + syear + " " + shour + ":" + sminute + ":" + ssecond;

                    if(!fileExistance(current_date))
                    {

                        File file = new File("data/data/com.example.mc.niezbednikglobtrotera/" + current_date);
                        if (!file.exists()) {
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    try {
                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput
                                                                (current_date, Context.MODE_PRIVATE));
                       for(int i = 0; i < routes.size(); i++)
                       {
                           toJSON = toJSON + routes.get(i) +",";

                       }

                        outputStreamWriter.write(toJSON);
                        outputStreamWriter.close();
                        Toast.makeText(getActivity(), "Pomyślnie dodano trasę",Toast.LENGTH_LONG).show();
                    }
                    catch (IOException e) {
                        Log.e("Exception", "Błąd zapisu " + e.toString());
                    }


                    if(!fileExistance("names"))
                    {

                        File file = new File("data/data/com.example.mc.niezbednikglobtrotera/names");
                        if (!file.exists()) {
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    try {



                        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().openFileOutput("names", Context.MODE_APPEND));
                        JSONObject object = new JSONObject();
                        object.put("name", current_date).toString();
                        outputStreamWriter.write(object.toString() + "," + "\r\n");
                        outputStreamWriter.close();


                    }
                    catch (IOException e) {
                        Log.e("Exception", "File write failed: " + e.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });

            final Button read_button = (Button) v.findViewById(R.id.button2);
            read_button.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {


                    String ret = "";



                    try {
                        values.clear();
                        InputStream inputStream = getContext().openFileInput("names");

                        if (inputStream != null) {
                            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                            String receiveString = "";
                            StringBuilder stringBuilder = new StringBuilder();

                            while ((receiveString = bufferedReader.readLine()) != null) {
                                stringBuilder.append(receiveString);
                            }

                            inputStream.close();
                            stringBuilder.insert(0, "{ \"data\" : [");
                            stringBuilder.setLength(stringBuilder.length() - 1);
                            stringBuilder.append("]}");
                            ret = stringBuilder.toString();


                            JSONObject parentObject = new JSONObject(ret);
                            JSONArray parentArray = parentObject.getJSONArray("data");
                            for (int i = 0; i < parentArray.length(); i++) {

                                JSONObject finalObject = parentArray.getJSONObject(i);
                                values.add(finalObject.getString("name"));
                                Log.e("VALUES", values+"");

                            }


                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final String[] value = new String[values.size()];

                    for(int i=0; i< values.size();i++)
                    {
                        value[i] = values.get(i);
                    }



                    AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(getActivity());


                    alertdialogbuilder.setTitle("Wybór trasy");

                    alertdialogbuilder.setItems(value, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            if(routePoints.size() >=2) {

                                Polyline route = map.addPolyline(new PolylineOptions()
                                        .width(5)
                                        .color(Color.BLUE)
                                        .geodesic(true)
                                        .zIndex(0));
                                route.setPoints(routePoints);
                            }




                        }
                    });

                    AlertDialog dialog = alertdialogbuilder.create();

                    dialog.show();




                }
                });


        }

        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mTicker);
    }

    @Override
    public void onLocationChanged(Location location) {


        if(location!=null) {
            lat = location.getLatitude();
            lon = location.getLongitude();
            LatLng mapPoint = new LatLng(lat, lon);
            routePoints.add(mapPoint);
            drawRoute(map);
            routes.add("{ \"lat\": " + lat + ", \"lon\": " + lon + "}" );


        }
    }

    public void readFile (GoogleMap map) {

        String ret = "";
        final String file_name = "places";

        if (getContext() == null)
            return;

        try {
            InputStream inputStream = getContext().openFileInput(file_name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader;
                inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                stringBuilder.insert(0,"{ \"data\" : [");
                stringBuilder.setLength(stringBuilder.length() - 1);
                stringBuilder.append("]}");
                ret = stringBuilder.toString();

                lats.clear();
                lons.clear();
                texts.clear();
                categories.clear();


                JSONObject parentObject = new JSONObject(ret);
                JSONArray parentArray = parentObject.getJSONArray("data");
                for (int i = 0; i < parentArray.length(); i++) {

                    JSONObject finalObject = parentArray.getJSONObject(i);
                    lats.add(finalObject.getDouble("lat"));
                    lons.add(finalObject.getDouble("lon"));
                    texts.add(finalObject.getString("opis"));
                    categories.add(finalObject.getDouble("kategoria"));

                }



                for (int i = 0; i < lats.size(); i++)

                {

                    LatLng position = new LatLng(lats.get(i), lons.get(i));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(position);



                    if (categories.get(i) == 0)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker5));
                        markerOptions.title("Jedzenie");
                    }

                    else if (categories.get(i) == 1)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker6));
                        markerOptions.title("Natura");
                    }

                    else if (categories.get(i) == 2)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker7));
                        markerOptions.title("Transport");
                    }

                    else if (categories.get(i) == 3)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker8));
                        markerOptions.title("Warto zobaczyć");
                    }

                    else if (categories.get(i) == 4)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker11));
                        markerOptions.title("Wypoczynek");
                    }

                    else if (categories.get(i) == 5)

                    {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker12));
                        markerOptions.title("Nocleg");
                    }

                    markerOptions.snippet(texts.get(i));
                   Marker marker = map.addMarker(markerOptions);
                    mMarkerArray.add(marker);
                    map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

                        @Override
                        public boolean onMarkerClick(Marker marker) {

                                marker.showInfoWindow();

                            return true;
                        }

                    });
                }

                }

        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean fileExistance(String fname){
        File file = getContext().getFileStreamPath(fname);
        return file.exists();
    }

    public boolean onMarkerClick(final Marker marker) {

        marker.showInfoWindow();
        return true;
    }

    public void drawRoute(GoogleMap map)
    {
        if(routePoints.size() >=2) {
            Polyline route = map.addPolyline(new PolylineOptions()
                    .width(5)
                    .color(Color.MAGENTA)
                    .geodesic(true)
                    .zIndex(0));
            route.setPoints(routePoints);
        }
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }



}